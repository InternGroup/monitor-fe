### DEMO
#### 仪表盘
![仪表盘](https://gitee.com/InternGroup/monitor-fe/raw/master/demo/dashboard.png)
#### 用户管理
![用户管理](https://gitee.com/InternGroup/monitor-fe/raw/master/demo/user.png)
#### 阈值更新
![阈值更新](https://gitee.com/InternGroup/monitor-fe/raw/master/demo/threshold.png)

### 1. 下载安装node

https://nodejs.org/dist/v12.18.2/node-v12.18.2-x64.msi

### 2.  安装vue环境

```bash
# 更换npm镜像
npm config set registry http://registry.npm.taobao.org
# 安装webpack
npm install webpack -g
# 安装vue-cli
npm install vue-cli -g
```

### 3. 构建项目

```bash
# 克隆项目到本地
git clone https://gitee.com/InternGroup/monitor-fe.git

# 进入项目目录
cd monitor-fe

# 安装依赖
npm install
```
### 4. 启动项目
##### 启动模拟后台程序
在此[下载](https://cloud.189.cn/t/n6JJZfreUZnq)相应平台的模拟程序并启动

##### 启动前端项目

```bash
npm run dev
```

### 5. 前后端部署

##### 打包前端项目

```bash
# 生产环境下打包
npm run build:prod
```

项目目录下生成`dist`文件夹

```bash
$ ls
favicon.png  index.html  static/
$ ls static/
css/  fonts/  img/  js/
```

##### 安装nginx服务器

[下载](https://nginx.org/en/download.html)最新版本并安装，之后进入nginx根目录

```bash
# nginx目录结构
$ ls
conf/  contrib/  docs/  html/  logs/  nginx.exe*  temp/
```

清空nginx的`html`文件夹内所有内容，将上一步生成的`dist`目录下所有内容拷贝到nginx的`html`文件夹内。下一步修改nginx服务器配置：

修改nginx目录下`conf`文件夹内`nginx.conf`的server字段内容如下

```bash
server {
	# nginx服务器默认端口为80，可直接在浏览器输入localhost访问
    listen       80;
    server_name  localhost;

	location / {
        root   html;
        index  index.html index.htm;
	}

    # 后端服务的配置
    # 将所有 /api 的请求转发至后台服务器
    location /api/ {
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # 后端服务地址
        proxy_pass http://localhost:8080/;
    }
}
```

然后将nginx安装路径添加至环境变量。

##### 启动nginx服务

```bash
# 启动nginx
$ start nginx
# 查看nginx进程
$ tasklist /fi "imagename eq nginx.exe"
映像名称                       PID 会话名              会话#       内存使用
========================= ======== ================ =========== ============
nginx.exe                    12088 Console                    1     12,248 K
nginx.exe                     2452 Console                    1     12,608 K
```

启动好以后再将后台服务启动(上面提到的模拟程序或最终发布的jar包)，浏览器输入`localhost`并回车，完成项目部署。

##### 重启或关闭nginx服务

```bash
# 重启
$ nginx -s reload
# 关闭
$ nginx -s quit
```

