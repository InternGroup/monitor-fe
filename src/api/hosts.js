// 获取所有主机IP
export function getHosts(that) {
  return new Promise(resolve => {
    that.$axios.get('/api/host').then(resp => {
      resolve(resp.data)
    })
  })
}
