// 把毫秒字符串转化为秒字符串
export function mills2secs(timeStr) {
  return timeStr.substring(0, timeStr.length - 3)
}
