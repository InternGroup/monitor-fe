// 获取硬件信息
export function getHardware(that, formData, type) {
  return new Promise(resolve => {
    var text = ''
    if (type === 'cpu') {
      text = '# 的CPU使用率'
    } else if (type === 'io_read') {
      text = '# 的IO读'
    } else if (type === 'io_write') {
      text = '# 的IO写'
    } else if (type === 'mem') {
      text = '# 的内存使用率'
    }
    that.$axios.post(
      '/api/hardware',
      formData
    ).then((resp) => {
      that.$message({
        message: '正在刷新主机 #' + that.dashForm.host + text,
        type: 'success'
      })
      resolve(resp.data)
    })
  })
}
