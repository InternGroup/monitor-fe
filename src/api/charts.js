// 画数据图
export function drawChart(that, chartData, type, thr) {
  var timestamp = []
  var chart_data = []
  var title = ''
  var label = ''
  var th = 0
  if (type === 'cpu') {
    title = 'CPU使用率监控'
    label = 'CPU使用率'
    th = parseInt(thr.cpu)
    for (const v of chartData) {
      const d = new Date(parseInt(v.timestamp) * 1000)
      timestamp.push(d.toLocaleTimeString())
      chart_data.push(v.cpu)
    }
  } else if (type === 'io_read') {
    title = 'IO读监控'
    label = 'IO读 MB'
    th = parseFloat(thr.io_read)
    for (const v of chartData) {
      const d = new Date(parseInt(v.timestamp) * 1000)
      timestamp.push(d.toLocaleTimeString())
      chart_data.push(v.io_read)
    }
  } else if (type === 'io_write') {
    title = 'IO写监控'
    label = 'IO写 MB'
    th = parseFloat(thr.io_write)
    for (const v of chartData) {
      const d = new Date(parseInt(v.timestamp) * 1000)
      timestamp.push(d.toLocaleTimeString())
      chart_data.push(v.io_write)
    }
  } else if (type === 'mem') {
    title = '内存使用率监控'
    label = '内存使用率'
    th = parseInt(thr.mem)
    for (const v of chartData) {
      const d = new Date(parseInt(v.timestamp) * 1000)
      timestamp.push(d.toLocaleTimeString())
      chart_data.push(v.mem)
    }
  } else if (type === 'trans') {
    title = '业务数据监控'
    label = '放款金额'
    th = parseInt(thr.trans)
    console.log(th)
    for (const v of chartData) {
      const d = new Date(parseInt(v.timeStamp) * 1000)
      timestamp.push(d.toLocaleTimeString())
      chart_data.push(v.moneyAmount)
    }
  }

  var myChart = that.$echarts.init(that.$refs.allChart)
  var option = {
    backgroundColor: '#304156',
    title: {
      text: title,
      textStyle: {
        fontWeight: 'normal',
        fontSize: 16,
        color: '#F1F1F3'
      },
      left: '45%',
      top: '5%'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        lineStyle: {
          color: '#57617B'
        }
      }
    },
    legend: {
      icon: 'rect',
      itemWidth: 14,
      itemHeight: 5,
      itemGap: 13,
      data: [label],
      right: '4%',
      top: '5%',
      textStyle: {
        fontSize: 12,
        color: '#F1F1F3'
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      boundaryGap: false,
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      data: timestamp
    }, {
      axisPointer: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      axisTick: {
        show: false
      }
    }],
    visualMap: {
      show: false,
      pieces: [
        {
          gt: 0,
          lte: th,
          color: '#03d6d6'
        }, {
          gt: th,
          color: '#e91642'
        }]
    },
    yAxis: [{
      type: 'value',
      name: label,
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 14
        }
      },
      splitLine: {
        lineStyle: {
          color: '#57617B'
        }
      }
    }],
    series: [{
      name: label,
      type: 'line',
      smooth: true,
      symbol: 'circle',
      symbolSize: 5,
      showSymbol: false,
      lineStyle: {
        normal: {
          width: 1
        }
      },
      areaStyle: {
        normal: {
          color: new that.$echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(0, 136, 212, 0.3)'
          }, {
            offset: 0.8,
            color: 'rgba(0, 136, 212, 0)'
          }], false),
          shadowColor: 'rgba(0, 0, 0, 0.1)',
          shadowBlur: 10
        }
      },
      itemStyle: {
        normal: {
          color: 'rgb(0,136,212)',
          borderColor: 'rgba(0,136,212,0.2)',
          borderWidth: 12

        }
      },
      data: chart_data,
      markLine: {
        silent: false,
        lineStyle: {
          normal: {
            color: 'rgb(137,189,27)'
          }
        },
        data: [{
          yAxis: th
        }],
        label: {
          normal: {
            formatter: '预警值'
          }
        }
      }
    }]
  }
  myChart.setOption(option)
}

// 画硬件图
export function drawHardwareChart(that, hardwareChart) {
  var timestamp = []
  var cpu = []
  var io = []
  var mem = []
  for (const v of hardwareChart) {
    const d = new Date(parseInt(v.timeStamp) * 1000)
    timestamp.push(d.toLocaleTimeString())
    cpu.push(v.cpuInfo)
    io.push(v.io)
    mem.push(v.mem)
  }
  var myChart = that.$echarts.init(that.$refs.hardwareChart)
  var option = {
    backgroundColor: '#304156',
    title: {
      text: '硬件数据监控',
      textStyle: {
        fontWeight: 'normal',
        fontSize: 16,
        color: '#F1F1F3'
      },
      left: '45%',
      top: '5%'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        lineStyle: {
          color: '#57617B'
        }
      }
    },
    legend: {
      icon: 'rect',
      itemWidth: 14,
      itemHeight: 5,
      itemGap: 13,
      data: ['CPU', '内存', 'IO'],
      right: '4%',
      top: '5%',
      textStyle: {
        fontSize: 12,
        color: '#F1F1F3'
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [{
      type: 'category',
      boundaryGap: false,
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      data: timestamp
    }, {
      axisPointer: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      axisTick: {
        show: false
      }
    }],
    yAxis: [{
      type: 'value',
      name: '使用率（%）',
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#57617B'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 14
        }
      },
      splitLine: {
        lineStyle: {
          color: '#57617B'
        }
      }
    }],
    series: [{
      name: 'CPU',
      type: 'line',
      smooth: true,
      symbol: 'circle',
      symbolSize: 5,
      showSymbol: false,
      lineStyle: {
        normal: {
          width: 1
        }
      },
      areaStyle: {
        normal: {
          color: new that.$echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(137, 189, 27, 0.3)'
          }, {
            offset: 0.8,
            color: 'rgba(137, 189, 27, 0)'
          }], false),
          shadowColor: 'rgba(0, 0, 0, 0.1)',
          shadowBlur: 10
        }
      },
      itemStyle: {
        normal: {
          color: 'rgb(137,189,27)',
          borderColor: 'rgba(137,189,2,0.27)',
          borderWidth: 12

        }
      },
      data: cpu
    }, {
      name: 'IO',
      type: 'line',
      smooth: true,
      symbol: 'circle',
      symbolSize: 5,
      showSymbol: false,
      lineStyle: {
        normal: {
          width: 1
        }
      },
      areaStyle: {
        normal: {
          color: new that.$echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(0, 136, 212, 0.3)'
          }, {
            offset: 0.8,
            color: 'rgba(0, 136, 212, 0)'
          }], false),
          shadowColor: 'rgba(0, 0, 0, 0.1)',
          shadowBlur: 10
        }
      },
      itemStyle: {
        normal: {
          color: 'rgb(0,136,212)',
          borderColor: 'rgba(0,136,212,0.2)',
          borderWidth: 12

        }
      },
      data: io
    }, {
      name: '内存',
      type: 'line',
      smooth: true,
      symbol: 'circle',
      symbolSize: 5,
      showSymbol: false,
      lineStyle: {
        normal: {
          width: 1
        }
      },
      areaStyle: {
        normal: {
          color: new that.$echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(219, 50, 51, 0.3)'
          }, {
            offset: 0.8,
            color: 'rgba(219, 50, 51, 0)'
          }], false),
          shadowColor: 'rgba(0, 0, 0, 0.1)',
          shadowBlur: 10
        }
      },
      itemStyle: {
        normal: {

          color: 'rgb(219,50,51)',
          borderColor: 'rgba(219,50,51,0.2)',
          borderWidth: 12
        }
      },
      data: mem
    }]
  }
  myChart.setOption(option)
}
