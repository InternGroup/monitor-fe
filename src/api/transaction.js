// 获取硬件信息
export function getTransaction(that, formData) {
  return new Promise(resolve => {
    that.$axios.post(
      '/api/transaction',
      formData
    ).then((resp) => {
      that.$message({
        message: '正在刷新主机 #' + that.dashForm.host + '# 的业务数据',
        type: 'success'
      })
      resolve(resp.data)
    })
  })
}
